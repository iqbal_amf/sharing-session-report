suppressMessages({
  library(dplyr)
  library(ggplot2)
  library(ROSE)
  source(here::here("connection.R"))
  
})

##------------- Read Data --------------------
dfnew <- redshiftQuery("
with branch_weekly_installment as (select
branch_id,
date_trunc('week', installment_time)::date+interval'5days' cut_off,count(distinct borrower_id) jumlah_mitra,
count(distinct borrower_id)/nullif(count(distinct bp_id),0) avg_serving_ratio,
count(distinct case when presence='HADIR' then borrower_id else null end)/count(distinct borrower_id)::numeric avg_persen_kehadiran,
(count (distinct case when date_part(h,installment_time)<8 or date_part(h,installment_time)>=21 then installment_id end))::numeric/ count(distinct installment_id) avg_persen_anomaly_topsheet
from installment
where date_trunc('week',installment_time::date) >= '2018-09-03'::date
group by 1,2)

,branch_installment as (select
bi1.branch_id,
bi1.cut_off, bi1.jumlah_mitra,
avg(bi2.avg_serving_ratio) avg_serving_ratio,
avg(bi2.avg_persen_kehadiran) avg_persen_kehadiran,
avg(bi2.avg_persen_anomaly_topsheet) avg_persen_anomaly_topsheet
from branch_weekly_installment bi1
join branch_weekly_installment bi2 on bi1.branch_id=bi2.branch_id and bi2.cut_off::date>=(bi1.cut_off::date-interval'3weeks') and bi2.cut_off::date <= bi1.cut_off::date
group by 1,2,3
order by 1,2)

,disb as (select branch_id,branch, date_trunc('week', disbursement_date)::date+interval'5days' cut_off,sum(plafond) disb
from disbursement
where date_trunc('week',cut_off::date) >= '2018-09-03'::date
group by 1,2,3
order by 1,3 desc)

,disb_growth as (select
dg1.branch_id,
dg1.branch,
dg1.cut_off,
sum(dg2.disb) disb_l1,
lag(disb_l1,4) over (partition by dg1.branch order by dg1.cut_off) disb_l4,
((disb_l1-disb_l4)/nullif(disb_l4,0)) disbursement_growth
from disb dg1
join disb dg2 on dg1.branch_id = dg2.branch_id and dg2.cut_off::date>=(dg1.cut_off::date-interval'3weeks') and dg2.cut_off::date <= dg1.cut_off::date
group by 1,2,3)
        
,delta_oar as (select branch_id,branch, cut_off ,sum(outstanding) os, lag(os,4) over (partition by branch order by cut_off) os_l4,
sum(case when status_par>0 then outstanding else 0 end) oar, lag(oar,4) over (partition by branch order by cut_off) oar_l4,
oar_l4/nullif(os_l4,0) d4, oar/nullif(os,0) d1, d1-d4 delta_persen_outstanding_at_risk
from loan_snapshot_weekly
where date_trunc('week',cut_off::date) >= '2018-09-03'::date
group by 1,2,3
order by 1,2 desc)

,usia as (select branch_id, min(date_trunc('week', cast(disbursement_date as date))) first_disbursement
from disbursement
group by 1)

select dense_rank() over (partition by bi.branch_id order by bi.cut_off) as row,bi.cut_off,bi.branch_id, delta_oar.branch, jumlah_mitra,
case when jumlah_mitra <= 1000 then 1 when jumlah_mitra > 1000 and jumlah_mitra <= 2000 then 2 else 3 end category_jumlah_mitra,datediff('week', first_disbursement, bi.cut_off) usia_branch,avg_serving_ratio, avg_persen_kehadiran, delta_persen_outstanding_at_risk,disbursement_growth,avg_persen_anomaly_topsheet, d1 persen_oar
from branch_installment bi
join disb_growth dg using(branch_id, cut_off)
join delta_oar using(branch_id, cut_off)
left join usia using(branch_id)
where bi.cut_off >= '2018-10-01'::date
order by 2,1
")

##------------- Create Segment --------------------
dfsegment <- dfnew %>% 
  mutate(segment = paste(cut_off, "_",category_jumlah_mitra))


##------------- Interquartile Range and Labelling--------------------
dfstat = dfsegment %>% 
  group_by(segment,cut_off) %>% 
  summarise(avg_segment = mean(persen_oar),
            med_segment = median(persen_oar),
            std_segment = sd(persen_oar),
            q1 = quantile(persen_oar, prob = 0.25),
            q2 = quantile(persen_oar, prob = 0.5),
            q3 = quantile(persen_oar, prob = 0.75),
            iqr = IQR(persen_oar)
  )  %>% 
  mutate(batas = q3 + 1.5*iqr)

dfsegment  = dfsegment %>% 
  left_join(dfstat %>% 
              select(segment, batas), by =("segment")) %>% 
  mutate(flag = ifelse(persen_oar > batas, "BAD","GOOD"))


##------------- Data Preparation--------------------
dfsegment <- dfsegment %>% 
  mutate(usia_branch = usia_branch/52.1429,
         usia_branch = round(usia_branch, digits = 0),
         usia_branch = as.character(usia_branch))
dfsegment <- subset(dfsegment, select = -c(jumlah_mitra ,segment,batas))
dfsegment[is.na(dfsegment)] <- 0


##------------- Log Model All (all category_jumlah_mitra)--------------------
# dfsegment$flag <- factor(dfsegment$flag)
# 
# segment_log <- glm(flag ~., data = dfsegment %>% select(-c(cut_off,row,category_jumlah_mitra, branch, branch_id)), family = binomial)
# summary(segment_log)
# predictTrain = predict(segment_log, type = "response")
# table(dfsegment$flag, predictTrain >= 0.10)


##------------- Log Model Loop per--------------------
model_log <- list()
for (i in unique(dfsegment$category_jumlah_mitra)) {
  model_log[[i]] <- glm(flag ~., data = dfsegment[which(dfsegment$category_jumlah_mitra == i), ] %>% select(-c(cut_off,row,category_jumlah_mitra, branch, branch_id,segment)), family = binomial)
}

for (i in unique(dfsegment$category_jumlah_mitra)){
  predictTrain = predict(model_log[[i]], type = "response")
  table(dfsegment[which(dfsegment$category_jumlah_mitra == i), ]$flag, predictTrain >= 0.10)  
}